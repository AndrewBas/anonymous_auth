import 'package:flutter/material.dart';

class SingIn extends StatefulWidget {
  @override
  _SingInState createState() => _SingInState();
}

class _SingInState extends State<SingIn> {

  Future singInAnon() async {
    try{
      AuthResult result = await _auth.singInAnonymoslu();
      FireBaseUser user = result.user;
      return user;
    }catch(e){
      print(e.toString());
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('auth'),
      ),
      body: Container(
        child: RaisedButton(
          child: Text('sing in'),
          onPressed: () async{
            final FirebaseAuth _auts = FirebaseAuth.instance;
            dynamic result =  await singInAnon();
            if(result == null){
              print('error');
            }else{
              print("result");
            }
          },
        ),
      ),
    );
  }
}
